# docker-compose部署codefever

## 参数
- 无

## 注意
- 如果你希望使用 22 端口作为 Git 的 SSH 协议端口。 你需要在启动镜像前将宿主系统的 SSH 服务 端口 先修改成其他端口
- 如果服务异常你可以登录 Shell 去人工维护, 也可以直接重启容器重启服务
- 服务启动后尝试访问 http://127.0.0.1 或 http://<server ip> 来登录
- 默认管理员用户: root@codefever.cn, 密码: 123456。登录后请修改密码并绑定 MFA 设备
