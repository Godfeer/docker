# docker-compose部署Redis

## 参数
- `--appendonly yes`：开启redis 持久化
- `--requirepass 123456`：设置redis密码为：12356

## 注意
- redis默认是不支持远程连接，需要手动开启，在redis.conf文件中
- 开启远程连接，需要注释：`bind 127.0.0.1`
