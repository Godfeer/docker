# docker-compose部署禅道研发项目管理平台

## 参数
- `ADMINER_USER`：设置 Web 登录数据库管理员帐户
- `ADMINER_PASSWD`：设置 Web 登录数据库的管理员密码
- `BIND_ADDRESS`：
    1. 是否够允许数据库远程登录（不影响Adminer）
    2. 设置为fasle，则不绑定IP，即任意IP都能登录（my.cnf：bind-address = 0.0.0.0）
    3. 默认为true（my.cnf：bind-address = 127.0.0.1）
- `MYSQL_ROOT_PASSWORD`：设置 MySQL 数据库的ROOT用户密码

## 注意
- 禅道管理员帐户为 admin，默认初始化密码为 123456
- 如果未设置`MYSQL_ROOT_PASSWORD`参数，默认密码：123456
