# docker-compose部署jenkins

## 参数
- `TZ`：指定容器运行所属时区
- `network_mode`：网络模式（默认bridge）

## 注意
- 浏览器访问地址：http://ip:8080
- 查看并输入管理员密码
> 因为容器`/var/jenkins_home`目录已经映射到`./data/jenkins_home`
> 只需要查看`./data/jenkins_home/secrets/initialAdminPassword`文件即可
```bash
cat ./data/jenkins_homesecrets/initialAdminPassword
```
![](doc/1.png)

- 安装插件

![](doc/2.png)
  
- 创建用户

![](doc/3.png)

- 下载镜像失败解决办法
```json
{
  "registry-mirrors": [
    "https://1amwtzpr.mirror.aliyuncs.com",
    "https://docker.mirrors.ustc.edu.cn",
    "https://registry.docker-cn.com",
    "http://hub-mirror.c.163.com"
  ]
}
```
## 安装自动化构建和部署所需的插件
- Maven Integration
- Pipeline Maven Integration
- Gitlab
- SSH
- Publish Over SSH