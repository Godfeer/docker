# docker-compose
> - 利用docker-compose自动化编排容器搭建开发、运行环境
> - 例如：mysql、nginx....

### 命令
#### `docker-compose`命令必须在 docker-compose.yml 文件所在目录执行
- 启动：`docker-compose up -d`
- 停止：`docker-compose down`
- 查看日志：`docker logs -t -f --tail 100 mysql`
- 进入容器：`docker exec -it mysql bash`

### 注意
- master主分支适用于linux系统，Mac系统请移步：mac分支

### 端口表
gitea 35000
jenkins3.8 35001
jenkins 35001

### 内部共享网络
- docker network create -d bridge jenkinsnetwork
