# docker-compose部署pure-ftpd

## 参数
- `21:21`：ftp的默认端口
- `20000-20200:20000-20200`：被动模式端口
- `30000-30009:30000-30009`：主动模式端口
- `PUBLICHOST: localhost`：设置默认服务器名称

## 后续操作
#### 1. 进入容器
>下面命令二选一，第一条报错，请换成第一条
- `docker exec -it pure_ftp bash`
- `docker exec -it pure_ftp /bin/sh`

#### 2. 创建目录
- 进入容器创建目录
```shell script
mkdir  /home/ftpuser/ftp
```
- 放开权限
```shell script
chmod 777 /home/ftpuser/ftp
```

#### 3. 创建用户
- 创建用户数据并与用户目录映射
```shell script
pure-pw useradd ftp -u ftpuser -g ftpgroup -d /home/ftpuser/ftp
```
- 创建用户数据，必须执行，否则创建的用户不运
```shell script
pure-pw mkdb
```